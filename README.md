# BCODE

Version en desarrollo , pre producción. 
Esta versión será Free.

Descipción:
IDE Visual parra desarrollo HTML + JS BOOTSTRAP 2 y 3 
Máxima versón donde se despliega bien la visualización con Bootstrap es la 3.x .

Version4 Tambien funciona, pero hay algunas incompatibilidades con el render de CSS (Se esta trabajando en la integración de webkit2)

Desarrollado en Debian 10 con Lazarus+FreePascal.
Testeado en Fedora33 y Mint20 (Las librerias deben ser copiadas al directorio  de librerias XD , para las distros hijas de Don Debian , pueden utilziar los paquetes debs)

**Se incorpora servidor interno , escucha en puerto 7979, se utiliza para comunicarse entre el composer (DOM) y el IDE.**

**Dependencias** 
> https://gitlab.com/urnsudeveop/bcode-ide/-/tree/master/deps

**NOTA. Entre as dependencias, se requiere tener instalado xterm,ya que bcode lo necesita para el despliegue de terminal empotrada.**

WIKI PAGES
> https://gitlab.com/urnsudeveop/bcode-ide/-/wikis/home
